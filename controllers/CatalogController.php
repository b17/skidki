<?php


namespace app\controllers;


use app\models\Company;
use app\models\CompanyCategory;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class CatalogController extends ApplicationController
{
    public  $layout = 'catalog';

    public function actionIndex()
    {
        $query = Company::find()
            ->addOrderBy(['rating_total' => SORT_DESC])
            ->addOrderBy(['reviews_count' => SORT_DESC]);

        $currentCategory = null;
        if (\Yii::$app->request->getQueryParam('category')) {
            $currentCategory = CompanyCategory::findOne([
                'rewrite' => \Yii::$app->request->getQueryParam('category'),
                'city_id' => 4,
            ]);

            if (!$currentCategory) {
                throw new NotFoundHttpException();
            }
        }

        $countQuery = clone $query;
        $pagination = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => 21]);

        $companies = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('index', [
            'companies' => $companies,
            'pagination' => $pagination,
            'current_category' => $currentCategory
        ]);
    }
} 
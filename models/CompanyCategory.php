<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company_category".
 *
 * @property integer $id
 * @property string $name
 * @property string $rewrite
 * @property integer $sort_order
 * @property string $created_at
 * @property string $updated_at
 * @property integer $city_id
 * @property integer $parent_id
 * @property integer $logo_id
 * @property string $meta_title
 * @property string $meta_description
 * @property string $nameUk
 * @property string $meta_keywords
 *
 * @property CompanyCategory $parent
 * @property CompanyCategory[] $companyCategories
 * @property City $city
 * @property Media $logo
 * @property CompanyCompanycategory[] $companyCompanycategories
 * @property Company[] $companies
 * @property CompanySubcategory[] $companySubcategories
 * @property CompanycategoryCompany[] $companycategoryCompanies
 * @property CompanytagsCompanycategory[] $companytagsCompanycategories
 * @property CompanyTags[] $companytags
 * @property CompanytmpCompanycategory[] $companytmpCompanycategories
 * @property CompanyTmp[] $companytmps
 * @property ImportSynonym[] $importSynonyms
 */
class CompanyCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'rewrite', 'sort_order', 'created_at', 'updated_at', 'nameUk'], 'required'],
            [['sort_order', 'city_id', 'parent_id', 'logo_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'rewrite', 'meta_title', 'nameUk', 'meta_keywords'], 'string', 'max' => 255],
            [['meta_description'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'rewrite' => 'Rewrite',
            'sort_order' => 'Sort Order',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'city_id' => 'City ID',
            'parent_id' => 'Parent ID',
            'logo_id' => 'Logo ID',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'nameUk' => 'Name Uk',
            'meta_keywords' => 'Meta Keywords',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(CompanyCategory::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyCategories()
    {
        return $this->hasMany(CompanyCategory::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogo()
    {
        return $this->hasOne(Media::className(), ['id' => 'logo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyCompanycategories()
    {
        return $this->hasMany(CompanyCompanycategory::className(), ['companycategory_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['id' => 'company_id'])->viaTable('{companycategory_company}', ['companycategory_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanySubcategories()
    {
        return $this->hasMany(CompanySubcategory::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanycategoryCompanies()
    {
        return $this->hasMany(CompanycategoryCompany::className(), ['companycategory_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanytagsCompanycategories()
    {
        return $this->hasMany(CompanytagsCompanycategory::className(), ['companycategory_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanytags()
    {
        return $this->hasMany(CompanyTags::className(), ['id' => 'companytags_id'])->viaTable('{companytags_companycategory}', ['companycategory_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanytmpCompanycategories()
    {
        return $this->hasMany(CompanytmpCompanycategory::className(), ['companycategory_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanytmps()
    {
        return $this->hasMany(CompanyTmp::className(), ['id' => 'companytmp_id'])->viaTable('{companytmp_companycategory}', ['companycategory_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImportSynonyms()
    {
        return $this->hasMany(ImportSynonym::className(), ['related' => 'id']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property integer $id
 * @property string $name
 * @property string $rewrite
 * @property string $description
 * @property string $siteUrl
 * @property string $email
 * @property string $address
 * @property string $vkUrl
 * @property string $fbUrl
 * @property string $twUrl
 * @property string $created_at
 * @property string $updated_at
 * @property integer $is_enabled
 * @property double $rating_total
 * @property double $rating_position
 * @property double $rating_staff
 * @property double $rating_price
 * @property double $rating_service
 * @property integer $mainPhoto_id
 * @property integer $reviews_count
 * @property string $price
 * @property integer $city_id
 * @property string $seo_title
 * @property string $seo_description
 * @property integer $vip
 * @property integer $counter
 * @property double $latitude
 * @property double $longitude
 * @property integer $isModerated
 * @property string $descriptionUk
 * @property string $nameUk
 * @property string $addressUk
 * @property string $odnoklassnikiUrl
 * @property integer $hasDescription
 * @property integer $widgetUsage
 * @property string $company_user
 * @property string $company_user_phone
 * @property string $company_user_email
 * @property integer $CCUser_id
 * @property integer $noAnswer
 * @property integer $notExist
 * @property integer $is_cc_moderated
 * @property integer $isNominated
 * @property integer $nomination_votes
 * @property integer $ria_recommend
 *
 * @property Certificates[] $certificates
 * @property CompanyAddress[] $companyAddresses
 * @property CompanyPhone[] $companyPhones
 * @property CompanyViewCount[] $companyViewCounts
 * @property NominationVotes[] $nominationVotes
 * @property ViewUser[] $viewUsers
 * @property BrandCompany[] $brandCompanies
 * @property Media $mainPhoto
 * @property City $city
 * @property CompanyTmp[] $companyTmps
 * @property CompanyCallback[] $companyCallbacks
 * @property CompanyCompanycategory[] $companyCompanycategories
 * @property CompanyCategory[] $companycategories
 * @property CompanyCompanytags[] $companyCompanytags
 * @property CompanyTags[] $companytags
 * @property CompanyDiscount[] $companyDiscounts
 * @property CompanyPhoto[] $companyPhotos
 * @property CompanyReview[] $companyReviews
 * @property CompanyVideo[] $companyVideos
 * @property CompanycategoryCompany[] $companycategoryCompanies
 * @property CompanytagsCompany[] $companytagsCompanies
 * @property ImportCompany[] $importCompanies
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rewrite', 'created_at', 'updated_at', 'is_enabled', 'rating_total', 'rating_position', 'rating_staff', 'rating_price', 'rating_service', 'reviews_count', 'vip', 'counter', 'isModerated', 'hasDescription', 'widgetUsage', 'is_cc_moderated'], 'required'],
            [['description', 'descriptionUk', 'nameUk'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['is_enabled', 'mainPhoto_id', 'reviews_count', 'city_id', 'vip', 'counter', 'isModerated', 'hasDescription', 'widgetUsage', 'CCUser_id', 'noAnswer', 'notExist', 'is_cc_moderated', 'isNominated', 'nomination_votes', 'ria_recommend'], 'integer'],
            [['rating_total', 'rating_position', 'rating_staff', 'rating_price', 'rating_service', 'latitude', 'longitude'], 'number'],
            [['name', 'rewrite', 'siteUrl', 'email', 'address', 'price', 'seo_title', 'seo_description', 'addressUk', 'company_user', 'company_user_phone', 'company_user_email'], 'string', 'max' => 255],
            [['vkUrl', 'fbUrl', 'twUrl', 'odnoklassnikiUrl'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'rewrite' => 'Rewrite',
            'description' => 'Description',
            'siteUrl' => 'Site Url',
            'email' => 'Email',
            'address' => 'Address',
            'vkUrl' => 'Vk Url',
            'fbUrl' => 'Fb Url',
            'twUrl' => 'Tw Url',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'is_enabled' => 'Is Enabled',
            'rating_total' => 'Rating Total',
            'rating_position' => 'Rating Position',
            'rating_staff' => 'Rating Staff',
            'rating_price' => 'Rating Price',
            'rating_service' => 'Rating Service',
            'mainPhoto_id' => 'Main Photo ID',
            'reviews_count' => 'Reviews Count',
            'price' => 'Price',
            'city_id' => 'City ID',
            'seo_title' => 'Seo Title',
            'seo_description' => 'Seo Description',
            'vip' => 'Vip',
            'counter' => 'Counter',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'isModerated' => 'Is Moderated',
            'descriptionUk' => 'Description Uk',
            'nameUk' => 'Name Uk',
            'addressUk' => 'Address Uk',
            'odnoklassnikiUrl' => 'Odnoklassniki Url',
            'hasDescription' => 'Has Description',
            'widgetUsage' => 'Widget Usage',
            'company_user' => 'Company User',
            'company_user_phone' => 'Company User Phone',
            'company_user_email' => 'Company User Email',
            'CCUser_id' => 'Ccuser ID',
            'noAnswer' => 'No Answer',
            'notExist' => 'Not Exist',
            'is_cc_moderated' => 'Is Cc Moderated',
            'isNominated' => 'Is Nominated',
            'nomination_votes' => 'Nomination Votes',
            'ria_recommend' => 'Ria Recommend',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCertificates()
    {
        return $this->hasMany(Certificates::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyAddresses()
    {
        return $this->hasMany(CompanyAddress::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyPhones()
    {
        return $this->hasMany(CompanyPhone::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyViewCounts()
    {
        return $this->hasMany(CompanyViewCount::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNominationVotes()
    {
        return $this->hasMany(NominationVotes::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getViewUsers()
    {
        return $this->hasMany(ViewUser::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrandCompanies()
    {
        return $this->hasMany(BrandCompany::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainPhoto()
    {
        return $this->hasOne(Media::className(), ['id' => 'mainPhoto_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyTmps()
    {
        return $this->hasMany(CompanyTmp::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyCallbacks()
    {
        return $this->hasMany(CompanyCallback::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyCompanycategories()
    {
        return $this->hasMany(CompanyCompanycategory::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanycategories()
    {
        return $this->hasMany(CompanyCategory::className(), ['id' => 'companycategory_id'])->viaTable('{companycategory_company}', ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyCompanytags()
    {
        return $this->hasMany(CompanyCompanytags::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanytags()
    {
        return $this->hasMany(CompanyTags::className(), ['id' => 'companytags_id'])->viaTable('{companytags_company}', ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyDiscounts()
    {
        return $this->hasMany(CompanyDiscount::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyPhotos()
    {
        return $this->hasMany(CompanyPhoto::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyReviews()
    {
        return $this->hasMany(CompanyReview::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyVideos()
    {
        return $this->hasMany(CompanyVideo::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanycategoryCompanies()
    {
        return $this->hasMany(CompanycategoryCompany::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanytagsCompanies()
    {
        return $this->hasMany(CompanytagsCompany::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImportCompanies()
    {
        return $this->hasMany(ImportCompany::className(), ['company_id' => 'id']);
    }
}

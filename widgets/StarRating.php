<?php


namespace app\widgets;


use yii\bootstrap\Widget;

class StarRating extends Widget
{
    public $rating;

    public function run()
    {
        echo $this->render('//widgets/star_rating.php', [
            'rating' => $this->rating
        ]);
    }


} 
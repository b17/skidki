<?php


namespace app\widgets;


use app\models\CompanyCategory;
use yii\bootstrap\Widget;
use yii\caching\DbDependency;

class CategoryNavigation extends Widget
{
    public function run()
    {
        \Yii::beginProfile("category_navigation", 'widget');

        $currentCategory = null;
        if (\Yii::$app->request->getQueryParam('category')) {
            $currentCategory = CompanyCategory::findOne([
                'rewrite' => \Yii::$app->request->getQueryParam('category'),
                'city_id' => 4
            ]);
        }

        $query =  CompanyCategory::find()
            ->where(['city_id' => 4, 'parent_id' => $currentCategory ? $currentCategory->id : null])
            ->orderBy(['name' => SORT_ASC]);

        $categories = \Yii::$app->db->cache(function () use ($query) {
            return $query->all();
        }, 3600, new DbDependency(['sql' => 'SELECT max(updated_at) FROM company_category']));


        echo $this->render('//widgets/category_navigation.php', [
            'categories' => $categories,
            'currentCategory' => $currentCategory
        ]);

        \Yii::endProfile("category_navigation", 'widget');
    }

} 
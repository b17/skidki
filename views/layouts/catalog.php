<?
/* @var $this  \yii\web\View */
use yii\helpers\Html;
?>

<? $this->beginContent('@app/views/layouts/main.php') ?>
<div class="row">

    <div class="col-md-3">
        <?= \app\widgets\CategoryNavigation::widget() ?>
    </div>

    <div class="col-md-9">
        <?= $content ?>
    </div>

</div>
<? $this->endContent() ?>


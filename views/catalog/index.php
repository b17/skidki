<?
/* @var $this \yii\web\View */
use yii\helpers\Html;
?>

<? \yii\widgets\Pjax::begin(); ?>
<? foreach ($companies as $company): ?>
    <div class="col-sm-4 col-lg-4" >
        <? if($this->beginCache("company_{$company->id}_{$company->updated_at}", ['duration' => 3600])): ?>
            <div class="thumbnail comp-item" id="company_<?= $company->id ?>">
                <img alt="" src="<?= Html::encode(\app\helpers\Image::thumbnail('http://moemisto.ua/uploads/media/event/0001/02/thumb_1121_event_show_content.jpeg', 320, 150)) ?>">
                <div class="caption comp-item_caption">
                    <h4 class="pull-right label label-danger"></h4>
                    <h4 class="comp-item_name">
                        <a href="#"><?= Html::encode($company->name) ?></a>
                    </h4>
                    <p class="comp-item_desc">
                        <?= \yii\helpers\StringHelper::truncate(strip_tags($company->description), 100) ?>
                    </p>
                </div>
                <div class="ratings">
                    <p class="pull-right"><?= Html::encode($company->reviews_count) ?> отзывов</p>
                    <p>
                        <?= \app\widgets\StarRating::widget(['rating' => $company->rating_total])  ?>
                    </p>
                </div>
            </div>
            <? $this->endCache() ?>
        <? endif; ?>
    </div>
<? endforeach; ?>

    <div class="row">
        <?= \yii\widgets\LinkPager::widget(['pagination' => $pagination]) ?>
    </div>

<? \yii\widgets\Pjax::end() ?>
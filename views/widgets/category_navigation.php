<?
/* @var $this \yii\web\View */
use yii\helpers\Html;
?>

<? if ($currentCategory): ?>
    <p class="lead"><?= $currentCategory->name ?></p>
<? endif; ?>

<div class="list-group">
    <? foreach($categories as $category): ?>
    <a class="list-group-item <? if ($currentCategory && $currentCategory->id == $category->id): ?>active<? endif; ?>" href="<?= Html::encode(\yii\helpers\Url::toRoute(['', 'category' => $category->rewrite])) ?>">
        <?= Html::encode($category->name) ?>
    </a>
    <? endforeach; ?>
</div>
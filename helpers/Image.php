<?php


namespace app\helpers;


use Imagine\Image\ManipulatorInterface;

class Image
{
    public static function thumbnail($filename, $width, $height, $mode = ManipulatorInterface::THUMBNAIL_OUTBOUND)
    {
        $target = sprintf("/assets/thumb/%dx%d/%s/%s", (int) $width, (int) $height, substr(md5($filename), 0, 2), pathinfo($filename, PATHINFO_BASENAME));
        $path = \Yii::getAlias("@webroot" . $target);
        if (file_exists($path)) {
            return \Yii::getAlias("@web" . $target);
        }

        \Yii::beginProfile("thumbnail", 'image');
        if (!file_exists(dirname($path))) {
            mkdir(dirname($path), 0777, true);
        }

        \yii\imagine\Image::thumbnail($filename, $width, $height, $mode)
            ->save($path, ['quality' => 75]);

        $webPath = \Yii::getAlias("@web" . $target);
        \Yii::endProfile("thumbnail", 'image');

        return $webPath;
    }
} 
